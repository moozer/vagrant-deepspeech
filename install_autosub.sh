#!/usr/bin/env bash

# start here: https://github.com/abhirooptalasila/AutoSub

AS_DIR="/opt/AutoSub"
AS_REPO="https://github.com/abhirooptalasila/AutoSub.git"

apt-get install -y python3-pip git ffmpeg

if [ ! -d $AS_DIR ]; then
	echo cloning from $AS_REPO to $AS_DIR
	cd /opt
	git clone $AS_REPO
fi

cd $AS_DIR
pip3 install --no-build-isolation -r requirements.txt

echo downloading scores and pbms
wget -N https://github.com/mozilla/DeepSpeech/releases/download/v0.8.2/deepspeech-0.8.2-models.pbmm
wget -N https://github.com/mozilla/DeepSpeech/releases/download/v0.8.2/deepspeech-0.8.2-models.scorer

