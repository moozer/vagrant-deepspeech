#!/usr/bin/env bash

AS_DIR="/opt/AutoSub"
SCRIPT="$AS_DIR/autosub/main.py"
PBMM="$AS_DIR/deepspeech-0.8.2-models.pbmm"
SCORER="$AS_DIR/deepspeech-0.8.2-models.scorer"

python3 $SCRIPT --model $PBMM --scorer $SCORER --file $1

